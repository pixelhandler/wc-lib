# WC Lib

A library of Web Components

## Status

WIP, experimental

## Components

See [packages/@wc/button](packages/@wc/button)

## Requirements

- Node.js

## Setup

- `npm install`

## Build

- `npm run build`

## Demo

- [ ] TODO actually make a demo

For now everything is manual :(

1. Build: `npm run build`
2. Use `http-server` via node, `npm install http-server -g`
3. Run `http-server` in the root of the project
4. visit http://localhost:8080/packages/%40wc/button/ using Chrome
