/* global QUnit */
// import WCButton from '../src/button';

// const { module, test } = QUnit;

QUnit.module('WCButton', function() {
  QUnit.test('it can be instantiated with no data', function(assert: any) {
    let button: WCButton = new WCButton();
    assert.ok(button, 'button instance exists');
  });

  QUnit.test('wc-button tag can be inserted using innerHTML', function(assert: any) {
    document.getElementById('qunit-fixture').innerHTML = '<wc-button href=""></wc-button>';
    const btn = document.getElementsByTagName('wc-button');
    assert.ok(btn);
  });

  QUnit.test('wc-button uses href attribute as property', function(assert: any) {
    document.getElementById('qunit-fixture').innerHTML = '<wc-button href="#"></wc-button>';
    const btn: WCButton = document.querySelector('wc-button') as WCButton;
    assert.equal(btn.href, '#');
  });

  QUnit.test('wc-button uses innerText as text property', function(assert: any) {
    document.getElementById('qunit-fixture').innerHTML = '<wc-button href="#">Link</wc-button>';
    const btn: WCButton = document.querySelector('wc-button') as WCButton;
    assert.equal(btn.text, 'Link');
  });

  QUnit.todo('wc-button has a text attribute/property', function(assert: any) {
    document.getElementById('qunit-fixture').innerHTML = '<wc-button href="#" text="Link"></wc-button>';
    const btn: WCButton = document.querySelector('wc-button') as WCButton;
    assert.equal(btn.text, 'Link');
  });

  QUnit.test('wc-button has role attribute as link', function(assert: any) {
    document.getElementById('qunit-fixture').innerHTML = '<wc-button href="#">Link</wc-button>';
    const btn: WCButton = document.querySelector('wc-button') as WCButton;
    assert.equal(btn.getAttribute('role'), 'link');
  });

  QUnit.test('wc-button has tabindex attribute as zero', function(assert: any) {
    document.getElementById('qunit-fixture').innerHTML = '<wc-button href="#">Link</wc-button>';
    const btn: WCButton = document.querySelector('wc-button') as WCButton;
    assert.equal(btn.getAttribute('tabindex'), '0');
  });

  QUnit.test('wc-button has props method to set href and text properties', function(assert: any) {
    document.getElementById('qunit-fixture').innerHTML = '<wc-button href="#">Link</wc-button>';
    let btn: WCButton = document.querySelector('wc-button') as WCButton;
    assert.equal(btn.href, '#');
    assert.equal(btn.text, 'Link');
    btn.props = { href: 'http://yo.io', text: 'Yo.io'};
    btn = document.querySelector('wc-button') as WCButton;
    assert.equal(btn.href, 'http://yo.io');
    assert.equal(btn.text, 'Yo.io');
  });
});
