# @wc/button

A Button component

- Built with TypeScript using Broccoli build tools.
- CSS processing includes `autoprefixer`

## Usage

Import the HTML, Web Component:

```html
  <link rel="import" href="../dist/wc-button.html" id="wc-button-link">
```

Use the `wc-button` component in your markup:

```html
  <wc-button href="git@gitlab.com:pixelhandler/wc-lib.git">Gitlab Repo</wc-button>
```

The `href` attribute will become the `href` property of the element, likewise the `innerText` will become the `text` property of the element.

Set properties:

```javascript
let btn = document.querySelector('wc-button');
btn.props = {
  text: 'Build a Component',
  href: 'https://developers.google.com/web/fundamentals/architecture/building-components/'
};
```

The initial attributes for `href` and `text` are set as properties (of the same name):

```html
  <wc-button href="git@gitlab.com:pixelhandler/wc-lib.git" text="Gitlab Repo"></wc-button>
```

### Requirements

- Node.js

## Setup

- `npm install`

## Build

- `npm run build`

See [dist/wc-button]

## Test

- [ ] TODO setup QUnit runner with Testem

See [index.html](index.html) for a manual test/demo

## Development

See [src](src) files, edit in .html, .ts, .css files, build will pack into a single file for importing.

## Polyfills

- See https://www.webcomponents.org/polyfills

For now using the bower component to polyfill, see [index.html](index.html) example file,

- `<script src="/bower_components/webcomponentsjs/webcomponents-loader.js"></script>`
