const fs = require('fs');
const path = require('path');
const concat = require('broccoli-concat');
const Funnel = require('broccoli-funnel');
const mergeTrees = require('broccoli-merge-trees');
const typescript = require('broccoli-typescript-compiler').typescript;
const autoprefixer = require('broccoli-autoprefixer');
const env = process.env.BROCCOLI_ENV;

let nodes;
const SRC = 'src';

let style = new Funnel(SRC, { include: ['button.css'] });
style = autoprefixer(style, { browsers: ['last 2 versions']});
style = concat(style, {
  inputFiles: ['button.css'],
  outputFile: '/wc-button.css',
  sourceMapConfig: { enabled: false },
  header: '<style>',
  footer: '</style>',
});

const dom = new Funnel(SRC, { include: ['button.html'] });

let js = new Funnel(SRC, { include: ['**/*.ts'] });
js = typescript(js, { tsconfig: './tsconfig.json' });
js = concat(js, {
  inputFiles: ['button.js'],
  outputFile: '/wc-button.js',
  sourceMapConfig: { enabled: false },
  header: '<script>',
  footer: '</script>',
});

let component = mergeTrees([style, dom]);
component = concat(component, {
  headerFiles: ['wc-button.css'],
  inputFiles: ['button.html'],
  outputFile: '/wc-template.html',
  sourceMapConfig: { enabled: false },
  header: [
    '<!-- BEGIN WCButton Web Component',
    fs.readFileSync(path.resolve(__dirname, 'LICENSE.txt'), 'utf8'),
    '-->',
    '<template id="wc-button">'
  ].join('\n'),
  footer: '</template>',
});

component = mergeTrees([component, js]);
component = concat(component, {
  headerFiles: ['wc-template.html'],
  inputFiles: ['wc-button.js'],
  outputFile: '/wc-button.html',
  sourceMapConfig: { enabled: false },
  footer: '<!-- END WCButton Web Component -->',
});

nodes = component;

const TEST = 'test';

if (env === TEST) {
  let tests = new Funnel(TEST, { include: ['**/*.ts'] });
  tests = typescript(tests, { tsconfig: './tsconfig.test.json' });
  tests = concat(tests, {
    inputFiles: ['**/*.js'],
    outputFile: '/wc-button-tests.js',
    sourceMapConfig: { enabled: false },
  });
  const runner = new Funnel(TEST, { include: ['index.html'] });
  nodes = mergeTrees([runner, tests, component]);
}

module.exports = nodes;
