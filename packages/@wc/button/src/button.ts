import WCButtonProps from './types/props';

class WCButton extends HTMLElement {
  private _href: string;
  private _text: string;

  constructor() {
    super();
    const shadowRoot: ShadowRoot = this.attachShadow({mode: 'open'});
    let template: HTMLTemplateElement = document.getElementById('wc-button') as HTMLTemplateElement;
    if (!template) {
      let link: HTMLLinkElement = document.getElementById('wc-button-link') as HTMLLinkElement;
      template = link.import.querySelector('template');
    }
    const instance: Node = template.content.cloneNode(true);
    shadowRoot.appendChild(instance);
  }

  static get observedAttributes() {
    return ['href', 'text'];
  }

  connectedCallback(): void {
    if (!this.hasAttribute('role')) {
      this.setAttribute('role', 'link');
    }
    if (!this.hasAttribute('tabindex')) {
      this.setAttribute('tabindex', '0');
    }
    if (this.getAttribute('href').length && !this.hasOwnProperty('href')) {
      this.href = this.getAttribute('href');
    }
    this._upgradeProperty('href');
    if (this.innerHTML.length && !this.hasOwnProperty('text')) {
      this.text = this.innerHTML.trim();
    } else {
      this._upgradeProperty('text');
    }
    this._render();
  }

  get href() { return this._href || ''; }
  set href(uri) { this._href = uri; }

  get text() { return this._text || ''; }
  set text(prop) { this._text = prop; }

  set props(props: WCButtonProps) {
    this.href = props.href;
    this.text = props.text;
    window.requestAnimationFrame(this._render.bind(this));
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any): void {
    if (name === 'text' && !newValue) {
      this._text = this.href || 'loading...'
      this._render();
    }
  }

  private _render(): void {
    const link: HTMLAnchorElement = this.shadowRoot.querySelector('a') as HTMLAnchorElement;
    link.href = this.href;
    link.innerHTML = this.text;
  }

  private _upgradeProperty(prop: string): void {
    if (this.hasOwnProperty(prop)) {
      let value: string = this[prop];
      delete this[prop];
      this[prop] = value;
    }
  }
}

const init = function() {
  window.customElements.define('wc-button', WCButton);
};

if ((document as any).attachEvent ? document.readyState === "complete" : document.readyState !== "loading") {
  init()
} else {
  document.addEventListener('DOMContentLoaded', function () {
    const WC = (window as any).WebComponents;
    if (WC && !WC.ready) {
      document.addEventListener('WebComponentsReady', init);
    } else {
      init();
    }
  });
}

// export default WCButton
