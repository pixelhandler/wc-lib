type WCButtonProps = {
  href: string,
  text: string,
}

export default WCButtonProps;
